variables:
  GO_VERSION: "1.23"
  GOLANGCI_LINT_VERSION: "v1.60.3"
  CI_TEMPLATES_PROJECT_PATH: "gitlab-org/security-products/ci-templates"
  CI_TEMPLATES_PROJECT_REF: "master"
  GITLAB_ADVANCED_SAST_ENABLED: 'true'

.go:
  image: golang:$GO_VERSION
  stage: test

include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml
  - template: Secret-Detection.gitlab-ci.yml

go test:
  extends: .go
  script:
    - go get -t ./...
    - go install gotest.tools/gotestsum@latest
    - go install github.com/boumenot/gocover-cobertura@latest
    # run tests, create coverprofile, and create junit report
    - gotestsum --junitfile report.xml --format testname -- -race -coverprofile=coverage.txt -covermode atomic ./...
    # write coverage report
    - gocover-cobertura < coverage.txt > coverage.xml
    # extract coverage score
    - go tool cover -func coverage.txt
  coverage: '/total:.*\d+.\d+%/'
  artifacts:
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
      junit: report.xml

go lint:
  extends: .go
  image: golangci/golangci-lint:$GOLANGCI_LINT_VERSION
  script:
    - wget https://gitlab.com/$CI_TEMPLATES_PROJECT_PATH/-/raw/$CI_TEMPLATES_PROJECT_REF/.golangci.yml
    - golangci-lint run --verbose --out-format code-climate:codequality.json,colored-line-number
  artifacts:
    when: always
    expire_in: 1 month
    reports:
      codequality: codequality.json

goimports:
  extends: .go
  script:
    # pin goimports to v0.30.0, because later versions cause our pipelines to fail.
    # see https://gitlab.com/gitlab-org/gitlab/-/issues/523514#note_2381800508
    - go install golang.org/x/tools/cmd/goimports@v0.30.0
    - FMT_CMD='goimports -w -local gitlab.com/gitlab-org .'
    - eval "$FMT_CMD"
    - |
      if ! git diff --ignore-submodules=dirty --exit-code; then
        echo
        echo "Some files are not formatted. Please format with \`$FMT_CMD\`"
        echo 'See https://docs.gitlab.com/ee/development/go_guide/#code-style-and-format-1 for more information'
        exit 1
      fi

go mod tidy:
  extends: .go
  script:
    - wget https://gitlab.com/$CI_TEMPLATES_PROJECT_PATH/-/raw/$CI_TEMPLATES_PROJECT_REF/.lefthook/pre-push/go-mod-tidy
    - sh go-mod-tidy

danger-review:
  stage: test
  image: registry.gitlab.com/gitlab-org/security-products/danger-bot:$DANGER_BOT_VERSION
  needs: []
  rules:
    - if: $CI_MERGE_REQUEST_LABELS =~ /danger-disabled/
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^v\d$/'
      when: never
    - if: $DANGER_DISABLED
      when: never
      # only run for GitLab projects, so people forking a project won't encounter danger errors
    - if: '$CI_COMMIT_BRANCH && $CI_PROJECT_NAMESPACE =~ /^gitlab-org/'
  variables:
    DANGER_BOT_VERSION: v0.12
  script:
    - BOLD_YELLOW='\033[1;33m'
    - NO_COLOR='\033[0m'
    - echo -e "${BOLD_YELLOW}Running danger-review job. If you need to disable this job, see https://gitlab.com/gitlab-org/security-products/ci-templates/#disabling-jobs${NO_COLOR}"
    - cp -r /danger/ danger/
    - mv danger/analyzers/Dangerfile Dangerfile
    - |
      if [ -f danger/Gemfile ]; then
        mv danger/Gemfile Gemfile
        bundle config set path danger/vendor/
        bundle exec danger --fail-on-errors=true
      else
        danger --fail-on-errors=true
      fi
