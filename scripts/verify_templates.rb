#!/usr/bin/env ruby

require 'net/http'
require 'json'

LINTER_URI = URI.parse "#{ENV['CI_API_V4_URL']}/projects/#{ENV['CI_PROJECT_ID']}/ci/lint"
STUB = <<YAML.freeze
  include:
    - #{ENV['CI_PROJECT_URL']}/raw/#{ENV.fetch('CI_COMMIT_REF_NAME', ENV['CI_DEFAULT_BRANCH'])}/includes-dev/stub.yml
YAML

def verify(file)
  content = STUB + IO.read(file)
  req = Net::HTTP::Post.new(LINTER_URI)
  req.form_data = { content: }
  # TODO: Change this to:
  # req.add_field(JOB-TOKEN, ENV['CI_JOB_TOKEN'])
  # when https://gitlab.com/gitlab-org/gitlab/-/issues/438781 has been completed
  req.add_field('PRIVATE-TOKEN', ENV['GITLAB_TOKEN'])
  response = Net::HTTP.start(LINTER_URI.hostname, LINTER_URI.port, use_ssl: true) { |http| http.request(req) }

  file = File.basename(file)

  json = JSON.parse(response.body)
  if json['valid']
    puts "\e[32mvalid\e[0m: #{file}" # Color 'valid' green
    return
  end

  puts ' '
  puts "Error encountered while validating file: #{file}"
  puts "Response code: #{response.code}, response message: #{response.message}"
  puts "JSON errors: #{json['errors']}" if json['errors']

  if response.code == '403'
    puts ' '
    puts "There was a permission error while accessing the URL #{LINTER_URI}."
    puts ' '
    puts 'Please ensure the GITLAB_TOKEN variable is configured to a Personal Access Token with API access ' \
         'or a Project Access Token with API and Developer access.'
    puts ' '
    puts 'See https://gitlab.com/gitlab-org/security-products/ci-templates/blob/89e1df4/CONTRIBUTING.md?plain=1#L14-27 for more details.'
    puts ' '
  end

  exit(1)
end

if ARGV.empty?
  Dir.glob("#{__dir__}/../**/*.yml").each { |file| verify(file) }

  # Given we test all the templates, the coverage is 100%, always. To showcase
  # how this is done, we print it here.
  # Regexp used to parse this: Coverage:\s(\d+)
  puts 'Coverage: 100%'
else
  verify(ARGV[0])
end
