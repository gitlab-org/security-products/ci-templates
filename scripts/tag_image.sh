#!/usr/bin/env bash

set -ex

SOURCE_IMAGE="$TMP_IMAGE$IMAGE_TAG_SUFFIX"
TARGET_IMAGE="$IMAGE_NAME:$IMAGE_TAG$IMAGE_TAG_SUFFIX"

echo "$CI_JOB_TOKEN" | docker login -u gitlab-ci-token --password-stdin "$CI_REGISTRY"
docker pull "$SOURCE_IMAGE"

TAG_OPTIONS=""
if [[ -n "$BUILD_MULTI_ARCHITECTURE_IMAGE" ]]; then
  echo "Tagging multi-architecture image"
else
  TAG_OPTIONS="--prefer-index=false"
  echo "Tagging single-architecture image"
fi

docker buildx imagetools create $TAG_OPTIONS "$SOURCE_IMAGE" --tag "$TARGET_IMAGE"

if [[ -n "$SEC_REGISTRY_IMAGE" && -n "$SEC_REGISTRY_PASSWORD" && -n "$SEC_REGISTRY_USER" ]]; then
  SEC_REGISTRY="${SEC_REGISTRY_IMAGE%%/*}"
  echo "$SEC_REGISTRY_PASSWORD" | docker login -u "$SEC_REGISTRY_USER" --password-stdin "$SEC_REGISTRY"
  TARGET_IMAGE="$SEC_REGISTRY_IMAGE:$IMAGE_TAG$IMAGE_TAG_SUFFIX"
  docker buildx imagetools create $TAG_OPTIONS "$SOURCE_IMAGE" --tag "$TARGET_IMAGE"
fi
