#!/usr/bin/env bash

set -ex

IMAGE_PLATFORM_SUFFIX=""
if [[ -n "$DOCKER_IMAGE_PLATFORM" ]]; then
  # convert linux/arm64 to -linux-arm64
  IMAGE_PLATFORM_SUFFIX="-${DOCKER_IMAGE_PLATFORM/\//-}"
fi
# shellcheck disable=SC2153 # (TMP_IMAGE is not a spelling mistake)
DOCKER_IMAGE_TAG="${TMP_IMAGE}${IMAGE_TAG_SUFFIX}${IMAGE_PLATFORM_SUFFIX}"
DOCKER_COMPRESSION_TYPE="gzip"
DOCKER_COMPRESSION_LEVEL="9"
DOCKER_FORCE_COMPRESSION="true"
DOCKER_BUILD_SBOM="$BUILD_MULTI_ARCHITECTURE_IMAGE"

# Define base annotations
ANNOTATION_ARGS=(
  "--annotation=org.opencontainers.image.revision=${CI_COMMIT_SHA}"
  "--annotation=org.opencontainers.image.source=${CI_PROJECT_URL}"
  "--annotation=org.opencontainers.image.url=${CI_PROJECT_URL}"
  "--annotation=org.opencontainers.image.title=${CI_PROJECT_TITLE}"
  "--annotation=org.opencontainers.image.vendor=GitLab"
)

if [ -f "CHANGELOG.md" ]; then
  # get most recent changelog version, including the leading v, for example: v5.8.2
  CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v/v/')
  if [ -n "$CHANGELOG_VERSION" ]; then
    echo "Setting annotation.org.opencontainers.image.version to latest version from CHANGELOG.md: '${CHANGELOG_VERSION}'"
    ANNOTATION_ARGS+=("--annotation=org.opencontainers.image.version=${CHANGELOG_VERSION}")
  else
    echo "CHANGELOG_VERSION is empty, skipping annotation"
  fi
else
  echo "CHANGELOG.md does not exist, skipping annotation"
fi

if [[ -n ${CI_PROJECT_DESCRIPTION:-} ]]; then
  ANNOTATION_ARGS+=("--annotation=org.opencontainers.image.description=${CI_PROJECT_DESCRIPTION}")
fi

docker buildx create --name analyzer-builder
echo "$CI_JOB_TOKEN" | docker login -u gitlab-ci-token --password-stdin "$CI_REGISTRY"

if [[ "${BUILD_MULTI_ARCHITECTURE_IMAGE}" != "true" ]]; then
  # default to linux/amd64 if building single architecture images
  DOCKER_IMAGE_PLATFORM="linux/amd64"
fi

if [[ "${BUILD_MULTI_ARCHITECTURE_IMAGE}" == "true" && "${DOCKER_IMAGE_PLATFORM}" == "" ]]; then
  echo "Combining previously built images into a single multi-architecture image"

  docker buildx imagetools create --tag "$DOCKER_IMAGE_TAG" \
    "${TMP_IMAGE}${IMAGE_TAG_SUFFIX}-linux-amd64" \
    "${TMP_IMAGE}${IMAGE_TAG_SUFFIX}-linux-arm64"
else
  echo "Building single-architecture image for ${DOCKER_IMAGE_PLATFORM}"

  if [[ "${BUILD_MULTI_ARCHITECTURE_IMAGE}" == "true" && "${DOCKER_EMULATE_PLATFORM}" != "" ]]; then
    # Sometimes, a base Docker image doesn't support the platform we want to build for. In this situation, emulation is required
    # For example, registry.gitlab.com/gitlab-org/gitlab-runner/go-fips:1.22.3 only supports linux/amd64, however, emulation allows us to build a linux/arm64 image
    #
    # Unfortunately, Go compilation is not stable when emulated, and can result in errors such as "/usr/local/go/pkg/tool/linux_amd64/compile: signal: segmentation fault (core dumped)"
    # Two work-arounds:
    #   1. Build the Go binary outside of Docker using Go cross-compilation tools (recommended)
    #      GOOS=$TARGETOS GOARCH=$TARGETARCH go build ... (run on the same platform as the base image)
    #      Use a COPY Docker command to add the binary to the image (Docker build run on a host native to the target platform)
    #   2. Run the Docker build on a host with the same platform as the base image. Go compilation is not emulated, so is stable. (current solution)
    #      The remainder of the Docker build is emulated, and slow
    #      Requires the job to be overridden by each project, e.g.
    #      fips build linux/arm64:
    #        tags: ["saas-linux-medium-amd64"]
    #        variables:
    #          DOCKER_EMULATE_PLATFORM: "linux/arm64"
    docker run --privileged --rm tonistiigi/binfmt --install "${DOCKER_EMULATE_PLATFORM}"
  fi

  # build a single-architecture image, either for stand-alone use, or so it can be combined later into a multi-architecture image
  docker buildx build \
    --build-arg GO_VERSION \
    --output="type=image,push=true,name=${DOCKER_IMAGE_TAG},compression=${DOCKER_COMPRESSION_TYPE},compression-level=${DOCKER_COMPRESSION_LEVEL},force-compression=${DOCKER_FORCE_COMPRESSION}" \
    "${ANNOTATION_ARGS[@]}" \
    --platform="${DOCKER_IMAGE_PLATFORM}" \
    --sbom="${DOCKER_BUILD_SBOM}" \
    --builder=analyzer-builder \
    --provenance=false \
    --file "$DOCKERFILE" .
fi

echo "Built docker image $DOCKER_IMAGE_TAG"
