package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/distribution/reference"
	"github.com/docker/cli/cli/manifest/types"
	"github.com/docker/cli/cli/registry/client"
	"github.com/docker/distribution"
	registrytypes "github.com/docker/docker/api/types/registry"
	"github.com/dustin/go-humanize"
)

// TODO: replace this file with a CI step.
// See https://gitlab.com/gitlab-org/gitlab/-/issues/496684 for details.

// BEGIN main.go
func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	cfg, err := LoadConfig()

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	registry := NewRegistry(cfg.registryHostName, cfg.username, cfg.password)
	manifest, err := registry.FetchManifest(ctx, cfg.dockerImageRef)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println()
	fmt.Println(manifest.Describe())
	fmt.Println()

	errs := manifest.ImageSizeLessThan(cfg.maxSizeBytes)

	if len(errs) > 0 {
		for _, err := range errs {
			fmt.Println(err)
		}

		os.Exit(1)
	}

	fmt.Printf("all images are within size limit of %s :-)\n", humanize.IBytes(uint64(cfg.maxSizeBytes)))
	fmt.Println()
}

// END main.go

// BEGIN pkg/manifest.go
type Manifest interface {
	Size() int64
	Describe() string
	ImageSizeLessThan(max int64) []error
}

// END pkg/manifest.go

// BEGIN pkg/image_manifest.go
type ImageManifest struct {
	image        reference.Named
	architecture string
	os           string
	config       distribution.Descriptor
	layers       []distribution.Descriptor
}

func NewImageManifest(image reference.Named, os, architecture string, config distribution.Descriptor, layers []distribution.Descriptor) *ImageManifest {
	return &ImageManifest{
		image:        image,
		architecture: architecture,
		os:           os,
		config:       config,
		layers:       layers,
	}
}

func (im *ImageManifest) Size() int64 {
	size := im.config.Size

	for _, layer := range im.layers {
		size += layer.Size
	}

	return size
}

func (im *ImageManifest) Describe() string {
	return fmt.Sprintf("image: %s\n  os: %s\n  architecture: %s\n  size: %s", im.image, im.os, im.architecture, humanize.IBytes(uint64(im.Size())))
}

func (im *ImageManifest) ImageSizeLessThan(max int64) []error {
	size := im.Size()

	if size < max {
		return []error{}
	}

	return []error{fmt.Errorf("image %s is %s over maximum size %s", im.image, humanize.IBytes(uint64(size-max)), humanize.IBytes(uint64(max)))}
}

// END pkg/image_manifest.go

// BEGIN pkg/registry.go
type Registry struct {
	registryHostName string
	password         string
	username         string
}

func NewRegistry(registryHostname, username, password string) *Registry {
	return &Registry{
		registryHostName: registryHostname,
		username:         username,
		password:         password,
	}
}

func (r *Registry) FetchManifest(ctx context.Context, image reference.Named) (Manifest, error) {
	authResolver := func(ctx context.Context, index *registrytypes.IndexInfo) registrytypes.AuthConfig {
		return registrytypes.AuthConfig{
			ServerAddress: r.registryHostName,
			Username:      r.username,
			Password:      r.password,
		}
	}

	registryClient := client.NewRegistryClient(authResolver, "gitlab.com/gitlab-org/security-products/ci-templates", false)
	return r.getManifest(ctx, registryClient, image)
}

func (r *Registry) getManifest(ctx context.Context, registryClient client.RegistryClient, image reference.Named) (Manifest, error) {
	manifests := make([]Manifest, 0)
	manifest, err := registryClient.GetManifest(ctx, image)

	if err != nil && strings.Contains(err.Error(), "is a manifest list") {
		return r.fetchManifestList(ctx, registryClient, image)
	}

	if err != nil {
		return nil, err
	}

	return NewManifests(append(manifests, r.buildManifest(manifest))), nil
}

func (r *Registry) fetchManifestList(ctx context.Context, registryClient client.RegistryClient, image reference.Named) (Manifest, error) {
	manifestList, err := registryClient.GetManifestList(ctx, image)

	if err != nil {
		return nil, err
	}

	manifests := make([]Manifest, len(manifestList))

	for i, manifest := range manifestList {
		manifests[i] = r.buildManifest(manifest)
	}

	return NewManifests(manifests), nil
}

func (r *Registry) buildManifest(manifest types.ImageManifest) Manifest {
	arch := "unknown"
	os := "unknown"

	if manifest.Descriptor.Platform != nil {
		arch = manifest.Descriptor.Platform.Architecture
		os = manifest.Descriptor.Platform.OS
	}

	if manifest.OCIManifest != nil {
		return NewImageManifest(manifest.Ref, os, arch, manifest.OCIManifest.Config, manifest.OCIManifest.Layers)
	}

	return NewImageManifest(manifest.Ref, os, arch, manifest.SchemaV2Manifest.Config, manifest.SchemaV2Manifest.Layers)
}

// BEGIN pkg/registry.go

// BEGIN pkg/manifests.go
type Manifests struct {
	manifests []Manifest
}

func NewManifests(manifests []Manifest) *Manifests {
	return &Manifests{manifests: manifests}
}

func (m *Manifests) Size() int64 {
	size := int64(0)

	for _, manifest := range m.manifests {
		size += manifest.Size()
	}

	return size
}

func (m *Manifests) Describe() string {
	descriptions := make([]string, len(m.manifests))

	for i, m := range m.manifests {
		descriptions[i] = m.Describe()
	}

	return strings.Join(descriptions, "\n")
}

func (m *Manifests) ImageSizeLessThan(max int64) []error {
	errs := make([]error, 0)

	for _, manifest := range m.manifests {
		for _, err := range manifest.ImageSizeLessThan(max) {
			errs = append(errs, err)
		}
	}

	return errs
}

// END pkg/manifests.go

// BEGIN config.go
type Config struct {
	dockerImageRef   reference.Named
	maxSizeBytes     int64
	registryHostName string
	username         string
	password         string
}

func LoadConfig() (*Config, error) {
	inputDockerImage := flag.String("docker-image", "", "The Docker image to check for size")
	inputMaxSizeBytes := flag.String("max_image_size", "", "The maximum number of bytes a Docker image can be")
	inputMaxSizeBytesMiB := flag.String("max_image_size_mib", "", "The maximum number of bytes a Docker image can be in MiB")

	flag.Parse()

	if inputDockerImage == nil || *inputDockerImage == "" {
		return nil, fmt.Errorf("docker-image is required")
	}

	dockerImageRef, err := reference.ParseDockerRef(*inputDockerImage)

	if err != nil {
		return nil, fmt.Errorf("failed to parse Docker image: %w", err)
	}

	maxSizeBytes, err := getMaxSizeBytes(inputMaxSizeBytes, inputMaxSizeBytesMiB)

	if err != nil {
		return nil, err
	}

	if maxSizeBytes <= 0 {
		return nil, fmt.Errorf("max image size must be greater than zero")
	}

	registryHostname := os.Getenv("DOCKER_REGISTRY")
	username := os.Getenv("DOCKER_REGISTRY_USERNAME")
	password := os.Getenv("DOCKER_REGISTRY_PASSWORD")

	if registryHostname == "" {
		return nil, fmt.Errorf("DOCKER_REGISTRY env var is required")
	}

	if username == "" {
		return nil, fmt.Errorf("DOCKER_REGISTRY_USERNAME env var is required")
	}

	if password == "" {
		return nil, fmt.Errorf("DOCKER_REGISTRY_PASSWORD env var is required")
	}

	cfg := &Config{
		dockerImageRef:   dockerImageRef,
		maxSizeBytes:     maxSizeBytes,
		registryHostName: registryHostname,
		username:         username,
		password:         password,
	}

	return cfg, nil
}

func getMaxSizeBytes(inputMaxSizeBytes, inputMaxSizeBytesMiB *string) (int64, error) {
	bytesToBytes := func(i int64) int64 { return i }
	mibToBytes := func(i int64) int64 { return i * 1024 * 1024 }

	osMaxImageSizeMiB := os.Getenv("CI_MAX_IMAGE_SIZE_MIB")

	if osMaxImageSizeMiB != "" {
		return getMaxSizeInput(osMaxImageSizeMiB, "CI_MAX_IMAGE_SIZE_MIB", mibToBytes)
	}

	osMaxImageSize := os.Getenv("CI_MAX_IMAGE_SIZE")

	if osMaxImageSize != "" {
		return getMaxSizeInput(osMaxImageSize, "CI_MAX_IMAGE_SIZE", bytesToBytes)
	}

	if inputMaxSizeBytesMiB != nil && *inputMaxSizeBytesMiB != "" {
		return getMaxSizeInput(*inputMaxSizeBytesMiB, "max size bytes in MiB", mibToBytes)
	}

	if inputMaxSizeBytes != nil && *inputMaxSizeBytes != "" {
		return getMaxSizeInput(*inputMaxSizeBytes, "max size bytes", bytesToBytes)
	}

	return 0, fmt.Errorf("max image size must be supplied")
}

func getMaxSizeInput(inputMaxSize string, description string, postProcessor func(int64) int64) (int64, error) {
	maxSize, err := strconv.ParseInt(inputMaxSize, 10, 64)

	if err != nil {
		return 0, fmt.Errorf("failed to parse %s: %w", description, err)
	}

	return postProcessor(maxSize), nil
}

// END config.go
