# A collection of `.gitlab-ci.yml` templates and includes for Security Products

[![build status](https://gitlab.com/gitlab-org/security-products/ci-templates/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/security-products/ci-templates/commits/master) [![coverage report](https://gitlab.com/gitlab-org/security-products/ci-templates/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/security-products/ci-templates/commits/master)

This is Security Products collection of [`.gitlab-ci.yml`](https://docs.gitlab.com/ee/ci/yaml/) file templates,
to be used in conjunction with [GitLab CI](https://docs.gitlab.com/ee/ci/).

These are for internal usage only. If you're looking for template to include in your projects to activate the Security Features, please refer to [the documentation](https://docs.gitlab.com/ee/user/application_security/).

## Upgrading golangci-lint

This project uses [golangci-lint](https://github.com/golangci/golangci-lint) to perform linting on all our analyzers. The [.golangci.yml](.golangci.yml) file is used to configure enabled linters.

To upgrade to a newer versio nof `golangci-lint`, follow these steps:

1. Upgrade the value of [GOLANGCI_LINT_VERSION](includes-dev/go.yml#L3). For example:

   ```yaml
   GOLANGCI_LINT_VERSION: "v1.60.3"
   ```

1. Upgrade the `golangci-lint` image tag in [lefthook.yml](lefthook.yml#L13) to use the same version configured in step `1`. For example:

   ```shell
   docker run --rm -it -w /app -v $(pwd):/app golangci/golangci-lint:v1.60.3 golangci-lint run \
     --verbose --config .git/info/lefthook-remotes/ci-templates/.golangci.yml
   ```

## Disabling jobs

The [danger-review](https://gitlab.com/gitlab-org/security-products/ci-templates/blob/a784f5d/includes-dev/go.yml#L70-93) job can be disabled using either of the following methods:

- For regular [branch pipelines](https://docs.gitlab.com/ee/ci/pipelines/pipeline_types.html#branch-pipeline):

   Run a [manual pipeline](https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually) for the branch in your `Merge Request` and set the `DANGER_DISABLED` CI variable to `true`.

- For [merge request pipelines](https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html):

   Add the [danger-disabled](https://gitlab.com/groups/gitlab-org/security-products/analyzers/-/labels?subscribed=&sort=relevance&search=danger-disabled) label to your MR and run a merge request pipeline.

## Folder structure

The `includes-dev` folder holds yaml files for development process and tools of the Security Product team.
See [include feature documentation](https://docs.gitlab.com/ee/ci/yaml/#include).

## Contributing guidelines

Please see the [contribution guidelines](CONTRIBUTING.md)
