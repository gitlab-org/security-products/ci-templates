## Developer Certificate of Origin + License

By contributing to GitLab B.V., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab B.V.
Except for the license granted herein to GitLab B.V. and recipients of software
distributed by GitLab B.V., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

_This notice should stay as the first item in the CONTRIBUTING.md file._

## Running a pipeline in a forked copy of this repository

The [test](https://gitlab.com/gitlab-org/security-products/ci-templates/blob/f54147e/.gitlab-ci.yml#L9-11) job of this project needs permission to execute the [CI Lint API](https://docs.gitlab.com/ee/api/lint.html#validate-sample-cicd-configuration) in order to verify that all included templates are valid.

If you fork this repository, you'll need to do the following in order for the [test](https://gitlab.com/gitlab-org/security-products/ci-templates/blob/f54147e/.gitlab-ci.yml#L9-11) job to succeed:

1. Create one of the following access token types:

   - [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with `api` scope.
   - [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) with `api` scope and `Developer` role.

1. Configure the [CI/CD Variables](https://docs.gitlab.com/ee/ci/variables/) in your forked project, `https://gitlab.com/<username>/ci-templates`, and add a new `GITLAB_TOKEN` with the value set to the `Access Token` created in step `1` above.

   Make sure to set the `Visibility` to `Masked` and _uncheck_ `Protect variable`.

## Issue tracker

To get support for your particular problem please use the
[getting help channels](https://about.gitlab.com/getting-help/).

The [GitLab issue tracker on GitLab.com][issue-tracker] is the right place for bugs and feature proposals about Security Products.
Please use the ~"Secure" and ~"devops:secure" labels when opening a new issue to ensure it is quickly reviewed by the right people.

**[Search the issue tracker][issue-tracker]** for similar entries before
submitting your own, there's a good chance somebody else had the same issue or
feature proposal. Show your support with an award emoji and/or join the
discussion.

Not all issues will be addressed and your issue is more likely to
be addressed if you submit a merge request which partially or fully solves
the issue. If it happens that you know the solution to an existing bug, please first
open the issue in order to keep track of it and then open the relevant merge
request that potentially fixes it.

[issue-tracker]: https://gitlab.com/gitlab-org/gitlab/issues
